(package-initialize)
(setq user-full-name "Drake M. Petersen"
      user-mail-address "drakemp@cs.umd.edu")

(require 'package)
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
	("melpa" . "http://melpa.org/packages/")
	("marmalade" . "http://marmalade-repo.org/packages/")
	("org" . "http://orgmode.org/elpa")))
(setq package-enable-at-startup nil)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))

;;add hook to other modes for 80 char limit
(defun fill-setup()
  "sets the column to 80 and sets minor mode auto-fill-mode"
  (set-fill-column 80)
  (auto-fill-mode t))

;;tramp
(use-package tramp
  :ensure t
  :init (setq tramp-default-method "sshx")
  )

;;(use-package tuareg
;;  :ensure t
;;  :init
;;  (use-package merlin
;;    :ensure t
;;    :init
;;    (let ((opam-share (ignore-errors (car (process-lines "opam" "config" "var" "share")))))
;;      (when (and opam-share (file-directory-p opam-share))
;;       ;; Register Merlin
;;       (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))
;;       (autoload 'merlin-mode "merlin" nil t nil)
;;       ;; Automatically start it in OCaml buffers
;;       (add-hook 'tuareg-mode-hook 'merlin-mode t)
;;       (add-hook 'caml-mode-hook 'merlin-mode t)
;;       ;; Use opam switch to lookup ocamlmerlin binary
;;       (setq merlin-command 'opam)))
;;
;;    ))

;;EVIL
(use-package evil
  :ensure t
  :init
  (evil-mode t)
  ;;Better Undo than default
  (setq evil-want-fine-undo t)
  ;;M-x l-c-d <----for color options
  (setq evil-insert-state-cursor '((bar . 4) "dark violet")
	evil-normal-state-cursor '(box "medium spring green"))
  )

;; Emacs transparency
(set-frame-parameter (selected-frame) 'alpha '(80 . 80))
(add-to-list 'default-frame-alist '(alpha . (80 . 80)))
(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
		    ((numberp (cdr alpha)) (cdr alpha))
		    ;; Also handle undocumented (<active> <inactive>) form.
		    ((numberp (cadr alpha)) (cadr alpha)))
               100)
	 '(80 . 80) '(100 . 100)))))
(global-set-key (kbd "C-c t") 'toggle-transparency)

;;(use-package challenger-deep-theme
;;  :ensure t
;;  :init (load-theme 'challenger-deep t))

;;Dashboard
(use-package dashboard
  :ensure t
  :init
  (setq show-week-agenda-p t)
  (setq dashboard-items '((recents  . 10)
                          (bookmarks . 15)
                          (agenda . 15)))
  ;;(add-to-list 'dashboard-items '(agenda) t)
  :config (dashboard-setup-startup-hook)
  )

;;tramp
(use-package tramp
  :ensure t
  :init (setq tramp-default-method "sshx")
  )

;;iedit
(use-package iedit
  :ensure t)
(use-package evil-iedit-state
  :ensure t)

(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode)
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))
  )

(use-package origami
  :ensure t
  :init (global-origami-mode t))

;;(use-package solidity-mode
;;  :ensure t)

(use-package company
  :ensure t
  :init (add-hook 'after-init-hook 'global-company-mode)
;;  (require 'color)
;;  
;;  (let ((bg (face-attribute 'default :background)))
;;    (custom-set-faces
;;     `(company-tooltip ((t (:background)))
;;     `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
;;     `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
;;     ;;`(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
;;     ;;`(company-tooltip-common ((t (:inherit font-lock-constant-face))))))
  )

;;eclim:
;;; This is for java developement in emacs via eclipse.
;; to install install eclim from eclim.org, when installing
;; ignore the vim files. 

;;(use-package eclim
;;  :ensure t
;;  :init
;;  (custom-set-variables
;;   '(eclim-eclipse-dirs '("~/eclipse"))
;;   '(eclim-executable "~/eclipse/plugins/org.eclim_2.7.2/bin/eclim"))
;;  (setq eclimd-autostart t)
;;  (defun my-java-mode-hook ()
;;    (eclim-mode t))
;;  (define-key eclim-mode-map (kbd "C-c C-c") 'eclim-problems-correct)
;;  (add-hook 'java-mode-hook 'my-java-mode-hook)
;;  (use-package company-emacs-eclim
;;    :ensure t
;;    :init (company-emacs-eclim-setup)
;;    )
;;  )

;;neotree
(use-package neotree
  :ensure t
  :init
  (global-set-key [f8] 'neotree-toggle))

;;expand-region
(use-package expand-region
  :ensure t
  :init
  ;;Note this binding is used because evil-mode is active
  ;;global-set-key is needed for users not using evil mode
  (define-key evil-motion-state-map (kbd "C-e") 'er/expand-region)
  )

;;basics all users should set
;; swap CTRL and CAPS (highly recommend)
(global-linum-mode)
(setq-default c-basic-offset 3)
(fset 'yes-or-no-p 'y-or-n-p)
;;auto refresh documents
(add-hook 'doc-view-mode-hook 'auto-revert-mode)
(setq auto-window-vscroll nil) ;;speed up?

;;multi-compile
(use-package multi-compile
  :ensure t
  :init
  (global-set-key (kbd "M-c") 'multi-compile-run)
  (setq multi-compile-alist
	'(
	  (c-mode . (("gcc-216" . "gcc -ansi -Wall -g -O0 -Wwrite-strings -Wshadow -pedantic-errors -fstack-protector-all %file-name")
		     ("gcc" . "gcc -g %file-name")
		     ("make" . "make")
		     ("mclean" . "make clean")))
	  (latex-mode . (("pdflatex" . "pdflatex %file-name")))
	  )
	;; more compile commands can be added here.
	)
  (defun bury-compile-buffer-if-successful (buffer string)
    "Bury a compilation buffer if succeeded without warnings "
    (when (and
	   (buffer-live-p buffer)
	   (string-match "compilation" (buffer-name buffer))
	   (string-match "finished" string)
	   (not
	    (with-current-buffer buffer
	      (goto-char (point-min))
	      (search-forward "warning" nil t))))
      (run-with-timer 1 nil
		      (lambda (buf)
			(bury-buffer buf)
			(switch-to-prev-buffer (get-buffer-window buf) 'kill))
		      buffer)))
  (add-hook 'compilation-finish-functions 'bury-compile-buffer-if-successful)
  )

(use-package org
  :ensure t
  :init
  (setq org-agenda-files '("/home/drakemp/Dropbox/"))
  (global-set-key (kbd "C-q") 'org-agenda)
  (use-package org-ref
    :ensure t
    :init
    (setq org-latex-pdf-process '("pdflatex %f" "bibtex %b" "pdflatex %f" "pdflatex %f")))
  (use-package org-bullets
    :ensure t
    :init 
    (add-hook 'org-mode-hook
	      (lambda ()
		(org-bullets-mode t)))
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((C . t)
       (ruby . t)
       (ocaml . t)
       (sh . t)
       (dot . t)
       (R . t)
       ))
    )
  (add-hook 'org-mode-hook 'fill-setup)
  )

;;latex
(add-hook 'latex-mode-hook 'fill-setup)

;;ace-mc
(use-package ace-mc
  :ensure t
  :init
  (global-set-key (kbd "M-f") 'ace-mc-add-multiple-cursors)
  )
;;never actually works
(defun add-pretty-symbols-wtf ()
  "make some word or string show as pretty Unicode symbols"
  (setq prettify-symbols-alist
        '(("lambda" . 955)))) ; λ

(add-pretty-symbols-wtf)
(global-prettify-symbols-mode)

;;(use-package tuareg
;;  :ensure t
;;  :init
;;  (add-to-list 'auto-mode-alist '("\\.mll\\'" . tuareg-mode))
;;  (add-to-list 'auto-mode-alist '("\\.mly\\'" . tuareg-mode))
;;  (use-package merlin
;;    :ensure t
;;    :init
;;;;    (add-hook 'tuareg-mode-hook 'merlin-mode)
;;    )
;;  )

;;smartparens
(use-package smartparens
  :ensure t
  :init
  (add-hook 'after-init-hook 'smartparens-global-mode)
  )
;;Emacs wiki
(defun window-swap-rotate ()
  "Swap the positions of this window and the next one."
  (interactive)
  (let ((other-window (next-window (selected-window) 'no-minibuf)))
    (let ((other-window-buffer (window-buffer other-window))
	  (other-window-hscroll (window-hscroll other-window))
	  (other-window-point (window-point other-window))
	  (other-window-start (window-start other-window)))
      (set-window-buffer other-window (current-buffer))
      (set-window-hscroll other-window (window-hscroll (selected-window)))
      (set-window-point other-window (point))
      (set-window-start other-window (window-start (selected-window)))
      (set-window-buffer (selected-window) other-window-buffer)
      (set-window-hscroll (selected-window) other-window-hscroll)
      (set-window-point (selected-window) other-window-point)
      (set-window-start (selected-window) other-window-start))
    (select-window other-window)))
(global-set-key (kbd "M-r") 'window-swap-rotate)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tango-dark)))
 '(delete-selection-mode nil)
;; '(eclim-eclipse-dirs (quote ("~/eclipse")))
;; '(eclim-executable "~/eclipse/plugins/org.eclim_2.7.2/bin/eclim")
 '(inhibit-startup-screen t)
 '(initial-scratch-message
   ";; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.
;; If you want to create a Org/Latex doc, use the code below.

#+title: TITLE
#+options: toc:nil num:nil tex:t tit
#+startup: latexpreview showall
#+latex_header:	\\usepackage{graphicx}  \\graphicspath{{}}
#+latex_header:	\\renewcommand{\\baselinestretch}{1}
#+latex_header:	\\newcommand\\tab[1][1cm]{\\noindent\\hspace*{#1}}
#+latex_header:	\\renewcommand{\\maketitle}{}
#+latex_header:	\\usepackage[top=0.5in, bottom=1in, left=1in, right=1in]{geometry}
#+latex_header:	\\usepackage{mathtools} \\DeclarePairedDelimiter\\ceil{\\lceil}{\\rceil} \\DeclarePairedDelimiter\\floor{\\lfloor}{\\rfloor}
* \\begin{center} Drake Petersen \\tab TITLE \\tab DATE 2018 \\end{center}
")
 '(org-agenda-files (quote ("~/Dropbox/")))
 '(org-agenda-span (quote fortnight))
 '(org-list-allow-alphabetical t)
 '(package-selected-packages
   (quote
    (org-ref 2048-game linum-relative origami folding neotree company-emacs-eclim challenger-deep-theme powershell flycheck use-package evil-iedit-state iedit w3m smartparens expand-region company multi-compile graphviz-dot-mode demo-it ace-mc prolog org-bullets evil ess)))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 90 :width normal))))
 '(region ((t (:background "light slate gray")))))
;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line
